现在想想，微信这东西真是让人又爱又恨，刚接触的时候，简直毫无头绪，不过在后台的配合下，现在终于能八九不离十的将微信获取用户信息和分享朋友圈这两块弄得比较透彻，得空了，赶紧将这些东西记下来，怕哪天又忘了，哈哈哈。

## 1、公众号。

要想做微信这东西，首先你得有公众号（在微信公众平台申请注册即可），主要是在"公众号设置"—>"功能设置"里填写如下的JS接口安全域名（注：登录后可在“开发者中心”查看对应的接口权限。比如说只有企业号才有分享朋友圈的功能，订阅号没有这个权限）。

![输入图片说明](https://git.oschina.net/uploads/images/2017/0930/172018_71c7f095_1036569.png "135651_vrFZ_2941696.png")



## 2、合理利用"开发者文档"。

![输入图片说明](https://git.oschina.net/uploads/images/2017/0930/172032_66a9fa59_1036569.png "135718_C6uB_2941696.png")

既然有这些功能，那微信提供的文档就不容小觑，对我们开发者最重要的自然就是"开发者文档"，而开发者文档最重要的则属于"微信网页开发"，"微信网页开发"的核心又在于"微信网页授权"、"微信JS-SDK说明文档"和"微信web开发者工具"。



### （1）微信网页授权

其实这一块只需了解即可，因为这一部分是属于后台研究的东西。我们只需要知道网页授权包括2种：手动授权（scope为snsapi_userinfo）和静默授权（scope为snsapi_base）。是需要手动授权还是静默授权，和后台商量好就行。



### （2）微信JS-SDK说明文档

这一块自然就是我们前端需要看的内容了，说多确实挺多，因为微信写的确实很详细，所用到的接口更是一一列举了出来，但我们只需要用哪个就复制哪一块就好了。那下面就开始具体说说JSSDK的使用步骤。



#### 步骤一：引入JS文件。

在需要调用JS接口的页面引入下面的JS文件：
```
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
```


#### 步骤二：获取微信授权。

有一个非常大的前提必须知道，在微信里打开的链接并不是我们普通的链接，而是加了很多前缀后缀的链接，比如说你的链接是：http://jojojojo.duapp.com/index.html?id=3

注：id=3是我要用的参数（因为个人项目需要，所以加了一个id），大家据具体情况而定，但必须注意的是后面只能加一个参数，无论你写几个，最后只留下第一个，如果有多个的话，可以用json的形式将2个合成一个，例如：param={id:3,scope:snsapi_userinfo}。



如果想你的链接能进行微信授权啥的，那就必须写成下面这个样子：

https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxef51b342984c9a03&redirect_uri=http%3a%2f%2fjojojojo.duapp.com%2findex.html%3fid%3d3&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect

注：appid就是你微信公众号的appid，url是编码之后的url。

编码网址：http://tool.chinaz.com/tools/urlencode.aspx。



在开发者工具打开这个链接后，会得到另外一个链接：

http://jojojojo.duapp.com/index.html?id=3&code=021v0NLa1mmCcr0g7IJa1bURLa1v0NLK&state=1

这个链接就是你真正要操作的链接，比如说我要通过ajax调用后台写好的获取微信授权的接口（例如："../share"），ajax调用时需要传2个字段给后台：code和url。这两个字段都是必须的，因为后台需要用code换取网页授权的access_token。url是页面完整的url，在这里url就是：

http://jojojojo.duapp.com/index.html?id=3&code=021v0NLa1mmCcr0g7IJa1bURLa1v0NLK&state=1



这个链接，要用location.href.split('#')[0]动态获取，而且最好需要encodeURIComponent()函数encode一下（在这里我没encode），因为页面一旦分享，微信客户端会在你的链接末尾加入其它参数，如果不是动态获取当前链接，将导致分享后的页面签名失败。如果再需要其他的参数，可以再加。



以我自己的链接为例，代码如下：

（下面代码的location.href就是上面的那个url）
```
var code=location.href.split("?")[1].split("&")[1].split("=")[1],   //code
    url=location.href.split('#')[0],                               //url
    id=location.href.split("?")[1].split("&")[0].split("=")[1],   //id，我自己的参数，你们据自己情况而定
    appId,nonceStr,timestamp,signature;

$.ajax({
	type:"post",
	url:"../share",
	async:true,
	dataType:"json",
	data:{
		code:code, 
		url:url   
	},
	success:function(data){
		console.log(data);
        var sp = data.signPackage,
        appId = sp.appId,
        nonceStr = sp.nonceStr,
        timestamp = sp.timestamp,
        signature = sp.signature;
	}),
	error:function(){
		console.log("fail");
	})
})
```
调用完即可获得你想要的信息（比如appId等），也就是log出来的data，如下图所示：

![输入图片说明](https://git.oschina.net/uploads/images/2017/0930/172108_d2ecdb66_1036569.png "140015_Fg4g_2941696.png")

注：appId、nonceStr、signature和timestamp这四个字段是做微信分享时必需的4个字段。userInfo里面的信息就更不用说了，拿到头像和用户名就可以做很多小game了。



#### 步骤三：分享到朋友圈或分享给朋友

上面才说的appId、nonceStr、signature和timestamp这四个字段，在这里立马就能派上用场了。我自己把这一块封装成了一个函数，所以要用时直接调用即可：
```
function weixin(tit,des){
	var redirect_uri="http://jojojojo.duapp.com/index.html?"+id;
	wx.config({
		debug: true, 
        // 开启调试模式，调用的所有api的返回值会在客户端alert出来，在pc端时会打印出来，不需要的话可以将true改成false。
		appId: appId,  // 必填，公众号的唯一标识
		timestamp: timestamp,  // 必填，生成签名的时间戳
		nonceStr: nonceStr,  // 必填，生成签名的随机串
		signature: signature,  // 必填，签名
		jsApiList: [
            // 必填，需要使用的JS接口列表
			"onMenuShareTimeline",
			"onMenuShareAppMessage"
		]
	});
    //分享到朋友圈
	wx.ready(function () {
	    wx.onMenuShareTimeline({
		    title: tit,   // 分享时的标题
		    link: "https://open.weixin.qq.com/connect/oauth2/authorize?appid="+appId+"&redirect_uri="+redirect_uri+"&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect",     // 分享时的链接
		    imgUrl: 'http://jojojojo.duapp.com/homepage/images/qr.png',    // 分享时的图标
		    success: function () {
			    console.log("分享成功");
		    },
		    cancel: function () {
			    console.log("取消分享");
		    }
	    });
        //分享给朋友
	    wx.onMenuShareAppMessage({
		    title: tit,
		    desc: des, 
		    link: "https://open.weixin.qq.com/connect/oauth2/authorize?appid="+appId+"&redirect_uri="+redirect_uri+"&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect",
		    imgUrl: 'http://jojojojo.duapp.com/homepage/images/qr.png',
		    type: '',
		    dataUrl: '', 
		    success: function () {
			    console.log("分享成功");
		    },
		    cancel: function () {
			    console.log("取消分享");
		    }
	    });
    })
}
```
调用如下：
```
var tit="分享啦",des='啦啦啦';

weixin(tit,des);
```


### （3）微信web开发者工具
![输入图片说明](https://git.oschina.net/uploads/images/2017/0930/172125_ca5e0fe5_1036569.png "140650_Ox3a_2941696.png")


临了，我得好好推荐一下这款工具，它通过模拟微信客户端的表现，使开发者可以更方便地在电脑上进行开发和调试。立即下载体验

具体操作：公众号登录管理后台，启用开发者中心，在开发者工具——web 开发者工具页面，向开发者微信号发送绑定邀请。

不会的可以去微信公众平台找步骤，偶了~

最后，如若哪里有问题，还请轻喷，但是欢迎指出大家一起交流(*^__^*) ！